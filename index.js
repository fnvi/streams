let stream = require('stream');
let readline = require('readline');

let fromFile = fileStream => {
    
    var rl = readline.createInterface({
        input: fileStream
    });

    var output = new stream.Readable({ objectMode:true, read:()=>null});
    rl.on('line', line => output.push(line));
    rl.on('close',()=>{output.push(null)});
    return output;
}

let fromCSVFile = fileStream => {
    
    var rl = readline.createInterface({
        input: fileStream
    });

    var output = new stream.Readable({ objectMode:true, read:()=>null});
    var headers = [];
    rl.on('line', line => {
        var values = line.split(',');
        if(headers.length)
            output.push(arrayCombine(headers,values));
        else
            headers = values;
    })
    rl.on('close',()=>{output.push(null)});
    return output;
}

let arrayCombine = (headers,values) => headers.reduce((output, header, index) => setValue(output,header,values[index]),{});


let array2stream = array => {
    var s = new stream.Readable({objectMode:true});
    array.map(obj => s.push(obj));
    s.push(null);
    return s;
}

let transformFactory = (method, options) => new stream.Transform({ objectMode:true, transform:(obj, encoding, cb) => cb( null , method(obj,options)) });

let buildFromMap = function (record,map){
    return Object.getOwnPropertyNames(map).reduce((output,mappedFieldName)=> {
        var currentFieldName = map[mappedFieldName];
        var value = record[currentFieldName];
        
        if(currentFieldName instanceof Array)
        {
            var m = currentFieldName[0];
            value = Object
                        .keys(m)
                        .reduce((output,mappedFieldName) => 
                            output.concat({name:mappedFieldName, id:record[m[mappedFieldName]]}
                        ),[]);
        }
        else if(currentFieldName instanceof Object){
            value = buildFromMap(record,map[mappedFieldName]);
        }
        return setValue(output,mappedFieldName,value)},{});
}

let setValue = (obj, field, value) => {obj[field] = value; return obj};

module.exports = {
    createStreamFromArray: array2stream,
    applySchema: Model => transformFactory(obj => new Model(obj)),
    toJSON: () => transformFactory(JSON.stringify),
    fromJSON: () => transformFactory(JSON.parse),
    fromFile: fromFile,
    fromCSVFile:fromCSVFile,
    toFile: () => transformFactory(data => data.toString()+'\n'),
    map: map => transformFactory(buildFromMap,map),
    transformFactory: transformFactory
};