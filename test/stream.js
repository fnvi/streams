let chai = require('chai');
let chaiStream = require('chai-stream');
chai.use(chaiStream);
let should = chai.should();
let expect = chai.expect;
let mongoose = require('mongoose');
let fs = require('fs');
let stream = require('stream');

let objectStream = require('../index');

describe('Stream creation',()=>{

    describe('createStreamFromArray(array)',()=>{

        it('should create a stream from an array',done =>{

            let array = new Array(5).fill({}).map((current,index)=> new Object({name:'Item '+index}));

            var stream = objectStream.createStreamFromArray(array);
            expect(stream).to.be.a.ReadableStream;
            var count = 0;

            stream.on('data',data => {
                data.should.be.an('object');
                data.should.have.property('name');
                count++;
            });

            stream.on('end',()=>{
                count.should.be.equal(array.length);
                done();
            });

        })
    });
});



describe('File usage',()=>{

    describe('fromFile(stream)',()=>{

        it('should read a file line by line',done =>{
            var s = objectStream.fromFile(fs.createReadStream('./test/data.txt'));

            expect(s).to.be.ReadableStream;

            s.on('data',data => {
                data.should.be.a('string');
                var obj = JSON.parse(data);
                obj.should.have.property('name');
            });

            s.on('end',function(){ done() });
        });
        
    });

    describe('fromCSVFile(stream)',()=>{
        it('should turn a csv file into a stream of objects', done =>{
            var s = objectStream.fromCSVFile(fs.createReadStream('./test/data.csv'));

            expect(s).to.be.a.ReadableStream;

            s.on('data',data=>{
                data.should.be.an('object');
            });

            s.on('end',done)
        });
    });

    

});

describe('Transforms',()=>{

    let createTestStream = number => objectStream.createStreamFromArray(new Array(number).fill({}).map((current,index)=> new Object({name:'Item '+(index+1)})))
    var testStream = createTestStream(5);

    beforeEach(done => {testStream = createTestStream(5), done();});

    describe('Transform a stream of objects into mongoose documents',()=>{

        let model = mongoose.model( 'testschema', new mongoose.Schema({ name:String }) );
        
        it('should transform a stream of objects into a stream of documents using a mongoose model',done=>{
            var out = testStream.pipe(objectStream.applySchema(model));
            
            out.on('data', data => {
                data.should.have.property('_id');
                data.should.have.property('name');
            });

            out.on('end',done);
        })
    });

    describe('toJSON()', ()=>{

        it('should call stringify on each object', done =>{
            var out = testStream.pipe(objectStream.toJSON());

            out.on('data', data => {
                data.should.be.a('string');
            });

            out.on('end',done);
        });
    });

    describe('fromJSON()',()=>{

        it('should convert JSON strings into objects', done =>{

            var s = new stream.Readable({objectMode:true});

            expect(s).to.be.a.Stream;

            s.push('{"name":"something"}');
            s.push(null);

            s.pipe(objectStream.fromJSON()).on('data',data=>{
                data.should.be.an('object');
                data.should.have.property('name','something');
            });

            s.on('end', done)
        });
    });

    describe('map(map)',()=>{

        let testMap = {
            newName:'name'
        };

        it('should transform the object based on the map', done =>{

            var output = testStream.pipe(objectStream.map(testMap)).on('data', data => {
                data.should.have.property('newName');
            });

            output.on('end',done);

        });
    });

    describe('toFile(filename)', ()=>{
        it('should modify the stream to delimit by newline', done =>{
            var output = testStream.pipe(objectStream.toJSON()).pipe(objectStream.toFile());

            output.on('data', data => data.endsWith('\n').should.equal(true) );

            output.on('end',done);
        });
    });

});

